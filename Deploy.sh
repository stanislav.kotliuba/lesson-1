###DIR VARIABLES_____
export apiProject=/var/www/dev.speedy.company/web
export apiRelease=$apiProject/jenkins/merge-test

##Cache dir permissions
cd $apiRelease/bootstrap
mkdir cache
cd cache
touch services.json
chmod -R 777 $apiRelease/bootstrap/cache/

##Symlink
ln -s -v $apiProject/.env.testing  $apiRelease/.env.testing
ln -s -v $apiProject/storage $apiRelease/storage


###INSTALL COMPOSER DEPENDENCIES_____
##Composer Install
cd $apiRelease
composer install

##Clear compiled
cd $apiRelease
php artisan clear-compiled
