###PARATEST_____
export TEST_REPORTS_DIR=/var/www/dev.speedy.company/web/jenkins/test-reports
cd /var/www/dev.speedy.company/web/jenkins/merge-test
vendor/bin/paratest -p 8 --runner WrapperRunner --max-batch-size 12 --log-junit $TEST_REPORTS_DIR/$BUILD_TIMESTAMP.xml
