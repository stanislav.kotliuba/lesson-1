###DIR VARIABLES_____
export apiProject=/var/www/dev.speedy.company/web
mv $apiProject/jenkins/merge-test $apiProject/releases/$BUILD_TIMESTAMP
export apiRelease=$apiProject/releases/$BUILD_TIMESTAMP

##Clear configs
cd $apiRelease
php artisan config:clear

##clearCompile
cd $apiRelease
php artisan clear-compiled

##Optimize
#cd $apiRelease
#php artisan optimize --force

##Cache routes
#cd $apiRelease
#php artisan route:cache

##Restart php
sudo /etc/init.d/php71-php-fpm restart

##Restart queue
#cd $apiRelease
#php artisan queue:restart

##Clear cache
cd $apiRelease
php artisan cache:clear


###ACTIVATE NEW RELEASE_____
##Relink Current Folder
rm $apiProject/current
ln -s $apiRelease $apiProject/current
ln -s $apiProject/.env $apiRelease/.env


##Restart php
sudo /etc/init.d/php71-php-fpm restart

##Restart supervisord
#sudo /etc/init.d/supervisord restart



###PURGE OLD RELEASES_____
##Remove Oldest Backup
cd $apiProject/releases
rm -rf $(ls -r | tail -n 1)
